//
//  ChartsDataSource.swift
//  ExamentUpaxiOS
//
//  Created by Charls Salazar on 14/04/21.
//

import UIKit

class ChartsDataSource: ASPCircleChartDataSource {
    var items: [Double] = []
    var mColors : [String] = []
    
    
    init(mItems: [Double], mColors: [String]){
        items = mItems
        self.mColors = mColors
    }
    
    @objc func numberOfDataPoints() -> Int {
        return items.count
    }
    
    @objc func dataPointsSum() -> Double {
        return items.reduce(0.0, { (initial, new) -> Double in
            return initial + new
        })
    }
    
    @objc func dataPointAtIndex(_ index: Int) -> Double {
        return items[index]
    }
    
    
    
    @objc func colorForDataPointAtIndex(_ index: Int) -> UIColor {
        switch index {
        case 0:
            return colorWithHexString(hex: mColors[0])
        case 1:
            return colorWithHexString(hex: mColors[1])
        case 2:
            return colorWithHexString(hex:  mColors[2])
        case 3:
            return colorWithHexString(hex:  mColors[3])
        case 4:
            return colorWithHexString(hex:  mColors[4])
        case 5:
            return colorWithHexString(hex: mColors[5])
        default:
            return colorWithHexString(hex: mColors[1])
        }
    }
    
    func colorWithHexString (hex:String) -> UIColor {

        var cString = hex.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines).uppercased()

        if (cString.hasPrefix("#")) {
            cString = (cString as NSString).substring(from: 1)
        }

        if (cString.count != 6) {
            return UIColor.gray
        }

        let rString = (cString as NSString).substring(to: 2)
        let gString = ((cString as NSString).substring(from: 2) as NSString).substring(to: 2)
        let bString = ((cString as NSString).substring(from: 4) as NSString).substring(to: 2)

        var r:CUnsignedInt = 0, g:CUnsignedInt = 0, b:CUnsignedInt = 0;
        Scanner(string: rString).scanHexInt32(&r)
        Scanner(string: gString).scanHexInt32(&g)
        Scanner(string: bString).scanHexInt32(&b)


        return UIColor(red: CGFloat(r) / 255.0, green: CGFloat(g) / 255.0, blue: CGFloat(b) / 255.0, alpha: CGFloat(1))
    }
}
