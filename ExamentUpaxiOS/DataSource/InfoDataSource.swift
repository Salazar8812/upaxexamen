//
//  InfoDataSource.swift
//  ExamentUpaxiOS
//
//  Created by Charls Salazar on 14/04/21.
//

import UIKit

protocol InfoDelegate : NSObjectProtocol{
    func selectOption(mOption: String)
}

class InfoDataSource: NSObject, UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate {
    var mListItem : [InfoUserModel] = []
    var tableView : UITableView?
    var mReferenceInfoUserItem : String = "ItemTableViewCell"
    var mReferenceCameraItem : String = "ItemCameraTableViewCell"
    var mViewController:  UIViewController?
    var mSettingsDelegate : InfoDelegate!
    var mPositionTag : Int?
    var mButton : UIView?
    var mField : UITextField!
    
    init(tableView: UITableView, mViewController:  UIViewController, mSettingsDelegate : InfoDelegate) {
        super.init()
        self.tableView = tableView
        self.tableView?.dataSource = self
        self.tableView?.delegate = self
        let nib1 = UINib(nibName: mReferenceInfoUserItem, bundle: nil)
        self.tableView?.register(nib1, forCellReuseIdentifier: mReferenceInfoUserItem)
        let nib2 = UINib(nibName: mReferenceCameraItem, bundle: nil)
        self.tableView?.register(nib2, forCellReuseIdentifier: mReferenceCameraItem)
        self.mViewController = mViewController
        self.mSettingsDelegate = mSettingsDelegate
    }
    
    func update(items: [InfoUserModel]) {
        self.mListItem = items
        self.tableView?.reloadData()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
       return 60
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.mListItem.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let item = self.mListItem[indexPath.row]
        
        if(item.mTypeAction == "Ingresar nombre" ){
            let cell = tableView.dequeueReusableCell(withIdentifier: mReferenceInfoUserItem, for: indexPath) as! ItemTableViewCell
            cell.mNameTextField.delegate = self
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: mReferenceCameraItem, for: indexPath) as! ItemCameraTableViewCell
            cell.mTitleLabel.text = item.mNameUser
            return cell
        }
    }
    
    
    @objc func listenerText(_ mTextField : UITextField){

    }

    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        do {
                    let regex = try NSRegularExpression(pattern: ".*[^A-Z a-z].*", options: [])
                    if regex.firstMatch(in: string, options: [], range: NSMakeRange(0, string.count)) != nil {
                        return false
                    }
                }
                catch {

                }
            return true
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.mSettingsDelegate.selectOption(mOption: mListItem[indexPath.row].mTypeAction!)
    }

}
