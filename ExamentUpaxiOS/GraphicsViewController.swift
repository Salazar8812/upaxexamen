//
//  GraphicsViewController.swift
//  ExamentUpaxiOS
//
//  Created by Charls Salazar on 14/04/21.
//

import UIKit

class GraphicsViewController: UIViewController, HomeDelegate {
    @IBOutlet weak var mGraphicCleanASPChart: ASPCircleChart!
    @IBOutlet weak var mGraphicSecurityASPChart: ASPCircleChart!
    
    @IBOutlet weak var mStatDoneLabel: UILabel!
    @IBOutlet weak var mStatNoLabel: UILabel!
    
    @IBOutlet weak var mStatDoneView: UIView!
    @IBOutlet weak var mStatNoView: UIView!
    
    @IBOutlet weak var mStatOneLabel: UILabel!
    @IBOutlet weak var mStatTwoLabel: UILabel!
    @IBOutlet weak var mStatThreeLabel: UILabel!
    @IBOutlet weak var mStatFourLabel: UILabel!
    @IBOutlet weak var mStatFiveLabel: UILabel!
    @IBOutlet weak var mStatSixLabel: UILabel!

    
    @IBOutlet weak var mStatOneView: UIView!
    @IBOutlet weak var mStatTwoView: UIView!
    @IBOutlet weak var mStatThreeView: UIView!
    @IBOutlet weak var mStatFourView: UIView!
    @IBOutlet weak var mStatFiveView: UIView!
    @IBOutlet weak var mStatSixView: UIView!
    
    var mCleanDataSource : ChartsDataSource!
    
    var mHomePresenter : HomePresenter!

    
    override func viewDidLoad() {
        super.viewDidLoad()
      
        getPresenter()
    }
    
    func getPresenter(){
        mHomePresenter = HomePresenter(mHomeDelegate: self)
        mHomePresenter.geData()
    }

    func populateCharts(mItems: [Double], mChartView: ASPCircleChart, mColors: [String]){
        mCleanDataSource = ChartsDataSource(mItems: mItems, mColors: mColors)
        mChartView.lineCapStyle = .round
        mChartView.latestSliceOnTop = false
        mChartView.dataSource = mCleanDataSource

    }
    
    func getData(mResponse: DataInfoUpaxResponse) {
        for item in mResponse.mQuestions{
            switch item.mText {
            case "¿Estaba limpia la sucursal?":
                mStatDoneLabel.text = item.mChartData[0].mText! + " " + String(item.mChartData[0].mPorcentaje!) + "%"
                mStatNoLabel.text = item.mChartData[1].mText! + " " + String(item.mChartData[1].mPorcentaje!) + "%"
                populateCharts(mItems: [Double(item.mChartData[0].mPorcentaje!),Double(item.mChartData[1].mPorcentaje!)], mChartView: mGraphicCleanASPChart, mColors: mResponse.mColores)
            break
            case "¿Qué empresas cuentan con medidas de seguridad?":
                mStatOneLabel.text = item.mChartData[0].mText! + " " + String(item.mChartData[0].mPorcentaje!) + "%"
                mStatTwoLabel.text = item.mChartData[1].mText! + " " + String(item.mChartData[1].mPorcentaje!) + "%"
                mStatThreeLabel.text = item.mChartData[2].mText! + " " + String(item.mChartData[2].mPorcentaje!) + "%"
                mStatFourLabel.text = item.mChartData[3].mText! + " " + String(item.mChartData[3].mPorcentaje!) + "%"
                mStatFiveLabel.text = item.mChartData[4].mText! + " " + String(item.mChartData[4].mPorcentaje!) + "%"
                mStatSixLabel.text = item.mChartData[5].mText! + " " + String(item.mChartData[5].mPorcentaje!) + "%"
                populateCharts(mItems: [Double(item.mChartData[0].mPorcentaje!),
                                        Double(item.mChartData[1].mPorcentaje!),
                                        Double(item.mChartData[2].mPorcentaje!),
                                        Double(item.mChartData[3].mPorcentaje!),
                                        Double(item.mChartData[4].mPorcentaje!),
                                        Double(item.mChartData[5].mPorcentaje!)],
                               mChartView: mGraphicSecurityASPChart, mColors: mResponse.mColores)

                break
            default:
                break
            }
        }
    }
}
