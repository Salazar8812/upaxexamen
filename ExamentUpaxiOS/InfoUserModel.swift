//
//  InfoUserModel.swift
//  ExamentUpaxiOS
//
//  Created by Charls Salazar on 14/04/21.
//

import UIKit

class InfoUserModel: NSObject {
    var mNameUser : String?
    var mTypeAction : String?
    
    init(mNameUser : String, mTypeAction : String){
        self.mNameUser = mNameUser
        self.mTypeAction = mTypeAction
    }

}
