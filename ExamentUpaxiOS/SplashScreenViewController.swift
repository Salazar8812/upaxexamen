//
//  SplashScreenViewController.swift
//  ExamentUpaxiOS
//
//  Created by Charls Salazar on 14/04/21.
//

import UIKit

class SplashScreenViewController: UIViewController {
    var mCounter = 0
    var mTimer = Timer()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        delayLaunchIntroductionView()
    }

    
    func delayLaunchIntroductionView(){
        mTimer.invalidate()
        mTimer = Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(actionDelay), userInfo: nil, repeats: true)
    }
    
    @objc func actionDelay() {
        mCounter += 1
        if(mCounter == 10){
            mTimer.invalidate()
            presentViewController(mNameStoryBoard: "Home", mNameViewController: "HomeViewController")
        }
    }
    
    func presentViewController(mNameStoryBoard : String, mNameViewController: String){
        let storyboard : UIStoryboard = UIStoryboard(name: mNameStoryBoard, bundle: nil)
        let vc : UIViewController = storyboard.instantiateViewController(withIdentifier: mNameViewController) as! UIViewController
        vc.modalPresentationStyle = .fullScreen
        let navigationController = UINavigationController(rootViewController: vc)
        navigationController.modalPresentationStyle = .overCurrentContext
        self.present(navigationController, animated: true, completion: nil)
    }
    
}
