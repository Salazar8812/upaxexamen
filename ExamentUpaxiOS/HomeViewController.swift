//
//  HomeViewController.swift
//  ExamentUpaxiOS
//
//  Created by Charls Salazar on 14/04/21.
//

import UIKit
import Photos
import Foundation

class HomeViewController: UIViewController, InfoDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    @IBOutlet weak var mListTableView: UITableView!
    var mInfoDataSource : InfoDataSource!
    var mListOptions : [InfoUserModel] = []
    var imagePicker: UIImagePickerController?
    let nombreImagen = "Foto"

    override func viewDidLoad() {
        super.viewDidLoad()
        populateOptions()
        createTableView()
    }
    
    func createTableView(){
        mInfoDataSource = InfoDataSource(tableView: mListTableView, mViewController: self, mSettingsDelegate: self)
        mListTableView.dataSource = mInfoDataSource
        mListTableView.delegate = mInfoDataSource
        mListTableView.reloadData()
        mInfoDataSource?.update(items: mListOptions)
    }

  
    func populateOptions(){
        mListOptions.append(InfoUserModel(mNameUser: "",mTypeAction: "Ingresar nombre"))
        mListOptions.append(InfoUserModel(mNameUser: "Tomar Selfie",mTypeAction: "Camara"))
        mListOptions.append(InfoUserModel(mNameUser: "Mostrar Graficas",mTypeAction: "Graficas"))

    }
    
    func selectOption(mOption: String) {
        print(mOption)
        switch mOption {
        case "Camara":
            capturar()
            break
        case "Graficas":
            pushViewController(mNameStoryBoard: "Graphics", mNameViewController: "GraphicsViewController")
            break
        default:
            break
        }
    }
    
    func pushViewController(mNameStoryBoard : String, mNameViewController: String){
        let vc = UIStoryboard.init(name: mNameStoryBoard, bundle: Bundle.main).instantiateViewController(withIdentifier: mNameViewController) as? UIViewController
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    
    
    func capturar() {
        let alert = UIAlertController(
            title: "Aviso",
            message: "¿Que acción desea realizar?",
            preferredStyle: .alert
        )
        var titulo = "Tomar Foto"
        let action1 = UIAlertAction(title: titulo, style: .default) { [weak self] _ in
            self!.openCamera()
        }
        titulo = "Visualizar"
        let action2 = UIAlertAction(title: titulo, style: .default) { [weak self] _ in
            self!.visualizar()
        }
      
        
        titulo = "Cancelar"
        let cancel = UIAlertAction(title: titulo, style: .destructive, handler: nil)
        alert.view.tintColor = UIColor.systemBlue
        alert.view.layer.cornerRadius = 25
        
        alert.addAction(action1)
        alert.addAction(action2)
        alert.addAction(cancel)
        present(alert, animated: true, completion: nil)
    }
    
    func openCamera(){
        if imagePicker == nil {
            if UIImagePickerController.isSourceTypeAvailable(.camera) {
                imagePicker = UIImagePickerController()
                imagePicker!.delegate = self
                imagePicker!.sourceType = .camera
                imagePicker!.allowsEditing = false
                present(imagePicker!, animated: true, completion: nil)
            } else {
               
            }
        }
    }
    
    func visualizar() {
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
            imagePicker = UIImagePickerController()
            imagePicker!.delegate = self
            imagePicker!.sourceType = .photoLibrary
            self.present(imagePicker!, animated: true, completion: nil)
        }
    }

    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        imagePicker!.dismiss(animated: true, completion: nil)
        if let imagen = info[UIImagePickerController.InfoKey(rawValue: UIImagePickerController.InfoKey.originalImage.rawValue)] as? UIImage {
            guardarImagen(img: imagen, nombre: nombreImagen)
        }
        imagePicker = nil
    }

    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        imagePicker!.dismiss(animated: true, completion: nil)
        imagePicker = nil
    }

    func guardarImagen(img: UIImage, nombre: String) {
        if let data = img.jpegData(compressionQuality: 0.6) {
            let filename = getDocumentsDirectory().appendingPathComponent(nombre)
            try! data.write(to: filename)
            print("guardada la imagen: \(filename)")
        }
    }
    
    func getDocumentsDirectory() -> URL {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return paths[0]
    }
    
    func leerImagen(nombre: String) -> UIImage? {
        let fileManager = FileManager.default
        let filename = getDocumentsDirectory().appendingPathComponent(nombre)
        if fileManager.fileExists(atPath: filename.path) {
            let image = UIImage(contentsOfFile: filename.path)
           print("leida la imagen: \(nombre)")
            return image
        } else {
            return UIImage()
        }
    }

}
