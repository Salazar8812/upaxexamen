//
//  HomePresenter.swift
//  ExamentUpaxiOS
//
//  Created by Charls Salazar on 14/04/21.
//

import UIKit
import Firebase

protocol HomeDelegate : NSObjectProtocol {
    func getData(mResponse : DataInfoUpaxResponse)
}

class HomePresenter: BasePresenter, AlamofireResponseDelegate {
    var mHomeDelegate : HomeDelegate!

    
    override init() {
        
    }
    
    init(mHomeDelegate : HomeDelegate){
        self.mHomeDelegate = mHomeDelegate
    }
    
    func geData(){
        RetrofitManager<DataInfoUpaxResponse>.init(requestUrl: ApiDefinitions.WS_GETINFO, delegate: self).requestGet()
    }
    
    func onRequestWs() {
        print("Iniciando...")
    }
    
    func onErrorLoadResponse(requestUrl: String, messageError: String) {
        print("Error al parsear los datos...")
    }
    
    func onErrorConnection() {
        print("Error de conexión...")
    }
    
    func onSuccessLoadResponse(requestUrl: String, response: BaseResponse) {
        print("Finalizado...")
        let mResponseModel = response as! DataInfoUpaxResponse
        print("Response: \(mResponseModel.toJSONString(prettyPrint: true) ?? "")")
        mHomeDelegate.getData(mResponse: mResponseModel)
    }
    
    func getDatafromFireBase(){
        
    }
    
}
