//
//  DataInfoUpaxResponse.swift
//  ExamentUpaxiOS
//
//  Created by Charls Salazar on 14/04/21.
//

import UIKit
import ObjectMapper

class DataInfoUpaxResponse: BaseResponse {
    var mColores : [String] = []
    var mQuestions : [Questions] = []
    
    override func mapping(map: Map) {
        mColores <- map["colors"]
        mQuestions <- map["questions"]
    }
}

class Questions : NSObject, Mappable {
    var mTotal : Int?
    var mText : String?
    var mChartData : [ChartData] = []
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        mTotal <- map["total"]
        mText <- map["text"]
        mChartData <- map["chartData"]
    }
    
    
}

class ChartData : NSObject, Mappable{
    var mText : String?
    var mPorcentaje : Int?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        mText <- map["text"]
        mPorcentaje <- map["percetnage"]
    }
}
