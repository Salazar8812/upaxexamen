//
//  BaseRetrofitManager.swift
//  ExamentUpaxiOS
//
//  Created by Charls Salazar on 14/04/21.
//

import UIKit
import Alamofire
import AlamofireObjectMapper
import ObjectMapper

open  class BaseRetrofitManager<Res : BaseResponse>: NSObject {
    
    open var requestUrl : String
    open var delegate : AlamofireResponseDelegate
    open var request : Alamofire.Request?
    
    public init(requestUrl: String, delegate : AlamofireResponseDelegate){
        self.delegate = delegate
        self.requestUrl = requestUrl
    }
    
    open var Manager: Alamofire.SessionManager = {
        let certificates = ServerTrustPolicy.certificates()
        
        let serverTrustPolicies: [String: ServerTrustPolicy] = [
            "https://us-central1-bibliotecadecontenido.cloudfunctions.ne": .disableEvaluation,
        ]
        
        let configuration = URLSessionConfiguration.default
        configuration.httpAdditionalHeaders = Alamofire.SessionManager.defaultHTTPHeaders
        
        let manager = Alamofire.SessionManager(
            configuration: URLSessionConfiguration.default,
            serverTrustPolicyManager: ServerTrustPolicyManager(policies: serverTrustPolicies)
        )
        
        return manager
    }()
    
    open func requestGet() {
        if BaseConnectionUtils.isConnectedToNetwork(){
            delegate.onRequestWs()
            request = Manager.request(self.requestUrl, method: HTTPMethod.get, encoding: JSONEncoding.default, headers: ApiDefinitions.CUSTOM_HEADERS).responseObject {
                (response: DataResponse<Res>) in
                switch response.result {
                case .success:
                    self.delegate.onSuccessLoadResponse(requestUrl : self.requestUrl, response: response.result.value! as Res)
                case .failure(_):
                    self.delegate.onErrorLoadResponse(requestUrl : self.requestUrl, messageError : "Se excedio el tiempo de espera: No fue posible establecer conexión con el servidor?")
                }
            }
        }else {
            delegate.onErrorConnection()
        }
    }
   
}


public protocol AlamofireResponseDelegate {
    
    func onRequestWs()
    
    func onSuccessLoadResponse(requestUrl : String, response : BaseResponse)
    
    func onErrorLoadResponse(requestUrl : String, messageError : String)
        
    func onErrorConnection()
    
}
