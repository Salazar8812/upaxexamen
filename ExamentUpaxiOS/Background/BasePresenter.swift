//
//  BasePresenter.swift
//  ExamentUpaxiOS
//
//  Created by Charls Salazar on 14/04/21.
//

import UIKit

class BasePresenter: NSObject {
    
    public override init(){
       
    }
    
    open func viewDidLoad(){
    }
    
    open func viewWillAppear(){
    }
    
    open func viewDidAppear(){
    }
    
    open func viewWillDisappear(){
    }
    
    open func viewDidDisappear(){
    }
    
    open func viewDidUnload(){
    }
    
}
