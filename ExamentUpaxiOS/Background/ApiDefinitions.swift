//
//  ApiDefinitions.swift
//  ExamentUpaxiOS
//
//  Created by Charls Salazar on 14/04/21.
//

import UIKit

class ApiDefinitions: NSObject {
    public static let CUSTOM_HEADERS = [
        "Content-Type": "application/json",
        "Accept" : "application/json"
    ]
        
    static let BASE_URL = "https://us-central1-bibliotecadecontenido.cloudfunctions.net/"
    static let WS_GETINFO =  BASE_URL + "helloWorld"
}
